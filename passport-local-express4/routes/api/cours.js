var express = require('express')

var router = express.Router()




var CoursController = require('../../controllers/cours.controller')
var ProfileController = require('../../controllers/profile.controller')

//problème : plus possible de protéger les routes avec checkifAuhthentified ? 
router.get('/displayCours', ProfileController.checkIfAuthentified, CoursController.getCours);
router.post('/addCours', CoursController.addCours);
router.get('/getCoursbyChap', CoursController.findCoursByChapitre);
router.get('/chapitres', CoursController.getChapitres)

  
module.exports = router;