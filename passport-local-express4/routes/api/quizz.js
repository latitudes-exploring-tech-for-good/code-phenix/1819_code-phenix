var express = require('express')
var router = express.Router()

var quizzController = require('../../controllers/quizz.controller')

router.post('/nouveau',quizzController.createQuizz)
router.get('/liste',quizzController.getQuizzes)
router.get('/affiche-quizz/:id',quizzController.getQuizz)
router.post('/nouvelle-question', quizzController.addQuestion)
router.post('/nouvelle-reponse',quizzController.addReponse)
router.post('/corrige-quizz',quizzController.corrigeQuizz)
router.post('/valider',quizzController.validerQuizz)

module.exports = router;