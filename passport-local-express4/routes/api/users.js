var express = require('express')

var router = express.Router()



  
// getting authentication controller and profile controller :
var AuthenticationController = require('../../controllers/authentication.controller')
var ProfileController = require('../../controllers/profile.controller')

/* GET users listing. */
router.post('/new', AuthenticationController.register);
router.post('/login',AuthenticationController.login);
router.get('/profile',ProfileController.profileRead);
  
module.exports = router;

