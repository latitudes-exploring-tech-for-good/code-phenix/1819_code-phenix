var express = require('express')

var router = express.Router()
var users = require('./api/users')
var cours = require('./api/cours')
var quizz = require('./api/quizz')

router.use('/users', users)
router.use('/cours', cours)
router.use('/quizz', quizz)

module.exports = router;