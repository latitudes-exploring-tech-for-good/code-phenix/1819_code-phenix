var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var passport = require('passport');

var indexRouter = require('./routes/index');


require('./models/db.model'); // config
require('./config/passport.config'); // config
require('./models/cours.model');
require('./models/quizz.model');

var app = express();


// Get the API route ...

var apiRouter = require('./routes/api.route');

/* 
var mongoose = require('mongoose');
try{mongoose.createConnection('mongodb://127.0.0.1:27017/codephenix')
    console.log(`Successfully Connected to the Mongodb Database  at URL : mongodb://127.0.0.1:27017/codephenix`)
}catch(err)
{ console.log(`Error Connecting to the Mongodb Database at URL : mongodb://127.0.0.1:27017/codephenix`)
} */

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:4200");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
  next();
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(passport.initialize()); // initialization of api routes

app.use('/api', apiRouter);

app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// error handlers
// Catch unauthorised errors
app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({"message" : err.name + ": " + err.message});
  }
});



module.exports = app;
