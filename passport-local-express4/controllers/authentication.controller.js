var passport = require('passport');
var mongoose = require('mongoose');
var User = mongoose.model('User');

// take the data from the submitted form and create a new Mongoose model instance
//call the setPassword method we created earlier to add the salt and the hash to the instance
// save the instance as a record to the database
// generate a JWT
// send the JWT inside the JSON response.


module.exports.register = function(req, res) { // registering function TODO : build service
    var user = new User();
  
    user.username = req.body.username;
    //user.email = req.body.email;
    user.admin = (req.body.admin == "true"); 
  
    user.setPassword(req.body.password);
  
    /*  user.save(function(err) {
      var token;
      token = user.generateJwt();
      res.status(200);
      res.json({
        "token" : token
      });
   */
      try{
        user.save(function(err) {
          if (err){
            return res.status(400).json({status : 400, message: err.message})
          }
          var token;
          token = user.generateJwt();
          return res.status(200).json({
            "token" : token,
            message: user.username + " " + user.admin,
            // ici on pourrait ajouter la date d'expiration de la session

          });
        })
      }catch(e){
        
        //Return an Error Response Message with Code and the Error Message.
        
        return res.status(400).json({status: 400, message: e.message})
    }
};

module.exports.login = function(req, res) {    // Login function : TODO : build service

  passport.authenticate('local', function(err, user, info){
      var token;
  
      // If Passport throws/catches an error
      if (err) {
        res.status(404).json({status: 400, message: err.message});
        return;
      }
  
      // If a user is found
      if(user){
        token = user.generateJwt();
        res.status(200);
        res.json({
          "token" : token,
          message : "Ca a marché !",
          // ici on pourrait ajouter la date d'expiration de la session
        });
      } else {
        // If user is not found
        res.status(200).json({
          "token" : [],
          message: "Non"
        });
      }
    })(req, res);
  
};