var mongoose = require('mongoose');
var Quizz = mongoose.model('Quizz');


module.exports.getQuizzes = async function(req,res) {
    try{
        var listenoms = await Quizz.find({},{nom:1})
        return res.status(200).json({data : listenoms, message : "nom des quizz bien recus"})
    } catch(e) {
        return res.status(400).json({status: 400, message: e.message});
    }
}

module.exports.getQuizz = async function(req,res){
    var quizzId = req.params.id;
    try{
        var quizz = await Quizz.findById(quizzId);
        return res.status(200).json({data: quizz, message: "quizz bien recu !"})
    } catch(e) {
        return res.status(400).json({status: 400, message: e.message});
    }
}

module.exports.createQuizz = function(req,res){
    var quizz = new Quizz()
    quizz.nom = req.body.nom;

    try{
        quizz.save(function(err) {
          if (err){
            res.status(400).json({status : 400, message: err.message})
          }
          res.status(200);
          res.json({
            data: quizz,
            message: "le quizz " + quizz.nom + " a été correctement ajouté"
          })
        })
      }catch(e){
        return res.status(400).json({status: 400, message: e.message})
    }
}

module.exports.addQuestion = async function(req,res){
    question = req.body.question
    //question.multipleChoice = Boolean(req.body.multipleChoice);
    var quizzId = req.body.quizzId
    try{        
        Quizz.findOneAndUpdate({"_id":quizzId},
        { $push : {questions : {question : question}}},
        {new : true},
        (err,quizz) => {return res.status(200).json({data : quizz})})
    }
    catch(e){
        return res.status(400).json({status: 400, message: e.message})
    }
}

module.exports.addReponse = async function(req,res){
    enonce = req.body.enonce;
    vraie = req.body.vraie;
    var quizzId = req.body.quizzId;
    var questionId = req.body.questionId;

    try{
        quizz = await Quizz.findById(quizzId);
 
        Quizz.findOneAndUpdate({"_id" : quizzId, 'questions._id' : questionId}, 
            {$push : {"questions.$.reponses" : {enonce : enonce, vraie : Boolean(vraie)}}},
            {new : true},
            (err,quizz) => {return res.status(200).json({data : quizz})})
    }
    catch(e){
        return res.status(400).json({status: 400, message: e.message})
    }
}

module.exports.corrigeQuizz = function(req,res){
    quizz = req.body.quizz;
    score = 0.0;
    total = quizz.questions.length
    try{
        for(let question of quizz.questions) {
            var correct = true
            for(let reponse of question.reponses) {
                if(!reponse.reponse){
                    reponse.reponse = false;
                }
                if(reponse.vraie !== reponse.reponse){
                    correct = false;
                }
            }
            if(correct){
                score ++;
                question.correction = true;
            }
            else { question.correction = false}
        }
        quizz.score = Number((score/total)*100)
        return res.status(200).json({message : "Quizz bien corrigé", data : quizz})
    }
    catch(e){
        return res.status(400).json({message : e.message})
    }
}

module.exports.validerQuizz = function(req,res) {
    quizz = req.body.quizz;
    try{
        Quizz.findOneAndUpdate({"_id" : quizz._id},quizz,{new : true},
        (err,quizz) => {return res.status(200).json({data: quizz,message : "Correctement validé"})})
    }
    catch(e) {
        res.status(400).json({message : e.message})
    }
}