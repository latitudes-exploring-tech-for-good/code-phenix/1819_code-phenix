var mongoose = require('mongoose');
var Cours = mongoose.model('Cours');

/* const coursTest1 = new Cours();
coursTest1.contenu = "<h2> Titre du cours <\h2>" +
"<p> Ceci est le <strong> contenu d'un cours <\strong> <\p> ";
const coursTest2 = new Cours();
coursTest2.contenu = "<h2> Titre du cours <\h2>" +
"<p> Ceci est le  contenu d'un <strong> autre <\strong>  cours <\p> ";

const coursTest = [coursTest1, coursTest2];

// la fonction listeCours était utilisée au début pour faire des tests sur l'affichage des cours
module.exports.listeCours = function(req, res) { 
    res.json(coursTest);
} */

// on utilise maintenant getCours pour récupérer les Cours dans la base de données

module.exports.getCours = async function(req, res , next){

    // mise en place des options de pagination mongoose
    var page =  1
    var limit =  100; 

    var options = {
        page,
        limit
    }
    // on récupère ensuite la liste des cours 
     try {
          var cours = await Cours.paginate({}, options)
  
          return res.status(200).json({status: 200, data: cours, message: "Les Cours ont été reçus"});

      } catch (e) {

          return res.status(400).json({status: 400, message: e.message});
    }
     
}

module.exports.findCoursByChapitre = async function(req, res , next){

   // 
   const chap = req.query.chapitre;
   console.log(chap);


     try {
         await Cours.find({ chapitre : chap }, function (err, cours){
  
          return res.status(200).json({status: 200, data: cours, message: "Les Cours ont été reçus"});})

      } catch (e) {

          return res.status(400).json({status: 400, message: e.message});
    }     
}

module.exports.addCours = function(req, res) { 
    var cours = new Cours();
  
    cours.contenu = req.body.contenu;
    cours.chapitre = req.body.chapitre;
    cours.titre = req.body.titre;

      try{
        cours.save(function(err) {
          if (err){
           return  res.status(400).json({status : 400, message: err.message})
          }else {
          return res.status(200).json({
            message: "le cours " + cours.titre + "a été correctement ajouté"
          })
        }
        })
      }catch(e){
        return res.status(400).json({status: 400, message: e.message})
    }
};

module.exports.getChapitres = async function(req,res) {
    try{
        var chapitres = await Cours.distinct("chapitre")
        return res.status(200).json({data : chapitres,message : "Les chapitres ont bien été récupérés"})
    }
    catch(e){
        return res.status(400).json({message : e.message})
    }
}



