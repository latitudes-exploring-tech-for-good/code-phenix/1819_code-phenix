var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({ //Definition of a mongoose model
    /* email: {
      type: String,
      unique: true,
      required: false
    }, */
    username: {
      unique: true,
      type: String,
      required: true
    },
    hash: String,
    salt: String,
    admin: { type: Boolean, // true si c'est un admin
          required: true}
  });

  userSchema.methods.setPassword = function(password) {  //method allowing to set a password (encrypt it)
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
  };

  userSchema.methods.validPassword = function(password) {  //verify that a user's password is right
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    return this.hash === hash;
  };

  userSchema.methods.generateJwt = function() {  // générer un jwt token
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);  // expiration date (in 7 days?)
  
    return jwt.sign({
      _id: this._id,
      //email: this.email, // probleme si l'utilisateur n'a pas d'email ? 
      username: this.username,
      admin: this.admin,
      exp: parseInt(expiry.getTime() / 1000),
    }, 'M3_Secret'); // DO NOT KEEP YOUR SECRET IN THE CODE!
  };

  var User = mongoose.model('User',userSchema); //convert userSchema into a model we can work with
  module.exports = User;