
var mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
//var Chapitre = mongoose.model('Chapitre');

var coursSchema = new mongoose.Schema({ //Definition of a mongoose model
     contenu: {
      type: String,
      unique: false,
      required: true
    },

    titre: {
      type: String,
      unique: true,
      required: true
    },

   // on devrait ajouter un numéro pour indiquer le rang 
  
    chapitre: {
      type: String,
      unique: false,
      required: false
    }


})

coursSchema.plugin(mongoosePaginate);
var Cours = mongoose.model('Cours',coursSchema); //convert userSchema into a model we can work with
module.exports = Cours;