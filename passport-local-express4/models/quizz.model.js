var mongoose = require('mongoose');

var ReponseSchema = new mongoose.Schema( {
    enonce : String,
    vraie : Boolean,
    reponse : Boolean,
})

var QuestionSchema = new mongoose.Schema( {
    question : String,
    correction : Boolean,
    reponses: [ReponseSchema],
    multipleChoices : Boolean,
})
var QuizzSchema = new mongoose.Schema( {
    nom : String,
    score : Number,
    questions: [QuestionSchema],
})

var Quizz = mongoose.model('Quizz',QuizzSchema);
module.exports = Quizz;