var mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

var chapitreSchema = new mongoose.Schema({ 
     titre: {
      type: String,
      unique: false,
      required: true
    },

    rang : {
      type: Float,
      unique: false,
      required: false
    },




})

chapitreSchema.plugin(mongoosePaginate);
var Chapitre = mongoose.model('Chapitre',chapitreSchema); 
module.exports = Chapitre;
