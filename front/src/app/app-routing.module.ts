import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import {AuthGuard} from './authguard.service';
import { AdminGuardService } from './admin-guard.service';
import { HomeAdminComponent } from './home-admin/home-admin.component';

const routes : Routes = [
  {path : 'login',component: LoginComponent},
  {path : 'home',  canActivate: [AuthGuard], component: HomeComponent},
  {path: 'home-admin', canActivate : [AdminGuardService], component: HomeAdminComponent},
  { path: '**', redirectTo: 'login'}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
}) 

export class AppRoutingModule { }
