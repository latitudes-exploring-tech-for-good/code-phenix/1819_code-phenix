import { Component, OnInit } from '@angular/core';
import { Cours } from "../cours";
import { HttpClient, HttpParams } from "@angular/common/http";
import {CoursService} from '../cours.service';




@Component({
  selector: 'app-accueil-user',
  templateUrl: './accueil-user.component.html',
  styleUrls: ['./accueil-user.component.css']
})
export class AccueilUserComponent implements OnInit {

  constructor(private coursService: CoursService, private http: HttpClient, 
    ) { }

    listeChapitre : string[];

    chapitreSelect : string;

    onSelect(chap : string): void{
      this.chapitreSelect = chap;
  }

  ngOnInit() {
    this.chapitreSelect = null;
    this.getChapitre(); 
  }

  // Récupère la liste des chapitres existant
  getChapitre() : void {
    this.coursService.getChapitres().subscribe(chapitres => this.listeChapitre = chapitres)
  }

}
