import { Component, OnInit, Input } from '@angular/core';
import {Cours} from '../cours';
import {CoursService} from '../cours.service';

@Component({
  selector: 'app-chapitre',
  templateUrl: './chapitre.component.html',
  styleUrls: ['./chapitre.component.css']
})
export class ChapitreComponent implements OnInit {

  @Input() chapitre : string;

  listeCours : Cours[];

  constructor(private coursService: CoursService) { }

  getCours(): void {
    console.log('on appelle la fontion getCours pour le chapitre');
    this.coursService.getCoursChapitre(this.chapitre).subscribe(data => {
      console.log(data);
    this.listeCours = data;}
       );
    console.log("la  liste des cours obtenus est " + this.listeCours);
  }

  ngOnInit() {
    this.getCours();
  }

}
