import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { map } from 'rxjs/operators';
import { Quizz } from './quizz';
import { Observable } from 'rxjs';
import {Response} from './response'

@Injectable({
  providedIn: 'root'
})
export class QuizzService {
  listeQuizzUrl : 'http://localhost:3000/api/quizz/liste'
  listeOfQuizz : Quizz []
  constructor( private http : HttpClient) { }

  getListeQuizz() : void {
    this.http.get<Response>('http://localhost:3000/api/quizz/liste').pipe(map(res => {return(res.data)} ))
    .subscribe(liste => {this.listeOfQuizz = liste});
  }

  getQuizz(quizzId: String) : Observable<Quizz> {
    return this.http.get<Response>('http://localhost:3000/api/quizz/affiche-quizz/'+quizzId).pipe(map(res => 
    {console.log(res.data);return(res.data)}))
  }

  corrigeQuizz(quizz : Quizz) : Observable<Quizz> {
    return this.http.post<Response>('http://localhost:3000/api/quizz/corrige-quizz/',{quizz : quizz})
    .pipe(map(res => {return res.data}))
  }

  creerQuizz(nom : string) : Observable<Quizz> {
    return this.http.post<Response>('http://localhost:3000/api/quizz/nouveau',{nom : nom})
    .pipe(map(res => {return res.data}))
  }

  ajoutQuestion(quizzId : string, question : string) : Observable<Quizz> {
    return this.http.post<Response>('http://localhost:3000/api/quizz/nouvelle-question',{quizzId : quizzId,question : question})
    .pipe(map(res => {return res.data}))
  }

  ajoutReponse(quizzId : string, questionId : string, enonce : string) : Observable<Quizz> {
    return this.http.post<Response>('http://localhost:3000/api/quizz/nouvelle-reponse',{quizzId : quizzId,enonce : enonce,questionId : questionId})
    .pipe(map(res => {return res.data}))
  }

  validerQuizz(quizz : Quizz) : Observable<Quizz>{
    return this.http.post<Response>('http://localhost:3000/api/quizz/valider',{quizz : quizz})
    .pipe(map(res => {return res.data}))
  }
}
