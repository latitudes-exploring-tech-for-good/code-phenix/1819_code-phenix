import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-admin',
  templateUrl: './home-admin.component.html',
  styleUrls: ['./home-admin.component.css']
})
export class HomeAdminComponent implements OnInit {

  displayComponent : String

  constructor(private authService: AuthenticationService, 
    private router: Router) { 
    }

    
  onSelect(string : String): void{
    // console.log("Résultat: ", this.displayComponent === "accueil"),
      this.displayComponent = string;
  }

  ngOnInit() {
    this.displayComponent = 'accueil'
  }

}
