import { Component, OnInit } from '@angular/core';
import { AuthenticationService}  from '../authentication.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import {User} from '../user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  model = new User();
  missingInformation = false;

  constructor(private authService: AuthenticationService, 
              private router: Router,
              private route : ActivatedRoute) {
  }

  login() {
      const val = this.model;

      if (val.username && val.password) {
          this.authService.login(val.username, val.password)
              .subscribe(
                  (res) => {
                      if(this.authService.isAdmin()){
                          this.router.navigateByUrl('/home-admin')
                      }
                      else{ 
                        this.router.navigateByUrl('/home');
                      }
                      this.authService.setSession(res);
                  }
              );  
      }
      else {
        this.missingInformation = true;
      }
  }
}
