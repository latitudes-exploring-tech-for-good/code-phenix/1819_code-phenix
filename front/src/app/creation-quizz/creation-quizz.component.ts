import { Component, OnInit } from '@angular/core';
import {Quizz} from '../quizz'
import { QuizzService } from '../quizz.service';

@Component({
  selector: 'app-creation-quizz',
  templateUrl: './creation-quizz.component.html',
  styleUrls: ['./creation-quizz.component.css']
})
export class CreationQuizzComponent implements OnInit {

  quizz = new Quizz();
  nouvellequestion : string;
  nouvellereponse : string;
  nomOk = false
  quizzAjoute = false ;
  constructor(private quizzService : QuizzService) { }

  ngOnInit( ) {
  }

  creerQuizz(){

    this.quizzService.creerQuizz(this.quizz.nom)
    .subscribe(quizz => this.quizz = quizz);
    this.nomOk = true;
  }

  ajouterQuestion(){
    this.quizzService.ajoutQuestion(this.quizz._id,this.nouvellequestion)
    .subscribe(quizz => {console.log(quizz),this.quizz = quizz} )
  }

  ajouterReponse(questionId : string){
    this.quizzService.ajoutReponse(this.quizz._id,questionId,this.nouvellereponse)
    .subscribe(quizz => this.quizz = quizz)
  }

  validerQuizz(){
    console.log(this.quizz)
    this.quizzService.validerQuizz(this.quizz).subscribe(quizz => {this.quizz = quizz});
    this.quizzAjoute = true;
  }
}
