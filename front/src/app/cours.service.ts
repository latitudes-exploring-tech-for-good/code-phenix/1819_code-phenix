import { Injectable } from '@angular/core';
import { Cours } from "./cours"
import { HttpClient, HttpParams } from "@angular/common/http";

import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import {Response} from './response'


@Injectable({
  providedIn: 'root'
})
export class CoursService {

  displayUrl = 'http://localhost:3000/api/cours/displayCours'
  createUrl = 'http://localhost:3000/api/cours/addCours'
  getCoursByChapterUrl = 'http://localhost:3000/api/cours/getCoursbyChap'
     
    constructor(private http: HttpClient) {
    }
       
    /* getCours(): Observable<Cours[]> {
        
        return this.http.get(this.displayUrl).pipe(map(res => { return res.json() as Cours[];
        }
      ))
    }  */

   getCours(): Observable<Cours[]> {
      return this.http.get<Cours[]>(this.displayUrl).pipe(map(res => {
        return res["data"].docs as Cours[];
    }));
  } 

  getChapitres(): Observable<string[]> {
    return this.http.get<Response>("http://localhost:3000/api/cours/chapitres")
    .pipe(map(res => {return res.data}))
  }

  createCours(cours: Cours): Observable<any>{
    //returns the observable of http post request 
    return this.http.post(this.createUrl, cours);
}

  getCoursChapitre(chap : string) :   Observable<Cours[]> {
  // definition des paramètres de la requête
  let params = new HttpParams().set('chapitre', chap );
  console.log(params.toString());

  return this.http.get<Response>('http://localhost:3000/api/cours/getCoursbyChap?chapitre='+ chap).pipe(map(res => {
    console.log(res.data); return(res.data);
}));
} 

   

}
