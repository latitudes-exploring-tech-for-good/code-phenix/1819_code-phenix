import { Component, OnInit, Input } from '@angular/core';
import {Quizz} from '../quizz'
import {QuizzService} from '../quizz.service'

@Component({
  selector: 'app-liste-quizz',
  templateUrl: './liste-quizz.component.html',
  styleUrls: ['./liste-quizz.component.css']
})
export class ListeQuizzComponent implements OnInit {

  display = 'liste';
  selectedQuizzId : String;
  constructor( public quizzService : QuizzService) { 
  }

  ngOnInit() {
    this.quizzService.getListeQuizz()
  }

  infos() {
    console.log(this.display)
  }
  
  liste(){
    this.display = 'liste'
  }

  selectedQuizz(id : string) : void {
    this.display = 'test'
    this.selectedQuizzId = id;
  }
}
