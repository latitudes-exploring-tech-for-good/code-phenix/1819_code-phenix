import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  // il faut ajouter une fonction qui fait appel à isLoggedIn pour rediriger l'utilisateur
  // vers une page de connection si il n'est pas connecté


  isadmin = localStorage.getItem('is_admin') === 'true' ;

  displayComponent : String;

    constructor() {
   }

  onSelect(string : String): void{
    this.displayComponent = string;
  }


  ngOnInit() {
    this.displayComponent = "accueil";
  }

}
