import { Component, OnInit } from '@angular/core';

import * as jwt_decode from "jwt-decode";
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  constructor( private authService : AuthenticationService,
    private router : Router) { }

  userInfo  = jwt_decode(localStorage.getItem("id_token"));

  username = this.userInfo.username;
  isadmin = localStorage.getItem('is_admin') === 'true' ;
  isuser = !this.isadmin;


  ngOnInit() {
  }

  deconnecte() {
    this.authService.logout()
    console.log("Déconnecté")
    this.router.navigate(['login'])
  }

}
