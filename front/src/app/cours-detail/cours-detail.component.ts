import { Component, OnInit, Input } from '@angular/core';
import {Cours} from '../cours';

@Component({
  selector: 'app-cours-detail',
  templateUrl: './cours-detail.component.html',
  styleUrls: ['./cours-detail.component.css']
})
export class CoursDetailComponent implements OnInit {

  @Input() cours: Cours;


  constructor() { }

  ngOnInit() {
  }

}
