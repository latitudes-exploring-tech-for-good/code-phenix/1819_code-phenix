import { ActivatedRouteSnapshot, CanActivate,Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import {AuthenticationService} from './authentication.service';

@Injectable()

export class AuthGuard implements CanActivate {
  constructor(private router : Router, private authservice : AuthenticationService){ };

  
  canActivate(
    
    
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      // si un utilisateur n'est pas authentifié, il est redirigé
      // à changer : il faudrait vérifier la signature du token pour éviter les fraudes 
      //(mais de toute façon les données sont protégées par le serveur)

      // problème : la fonction isLoggedIn ne marche pas, elle renvoie tjrs false  
    console.log(" l'utilisateur est " + this.authservice.isLoggedIn());
      
    if(!localStorage.getItem("id_token") || !(this.authservice.isLoggedIn()) )   {
      this.router.navigate(['/login'], { queryParams: { redirectUrl: state.url }});
      return false
    
    }
    // si il est admin, il est redirigé vers home-admin
    else if (this.authservice.isAdmin() ) {
      this.router.navigate(['/home-admin'], { queryParams: { redirectUrl: state.url }});
      console.log("Vous devez être un utilisateur classique pour accéder à cette route");
      return true
      }  
    else {
      return true
    }
    }
}

