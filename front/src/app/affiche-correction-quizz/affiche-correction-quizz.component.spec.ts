import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AfficheCorrectionQuizzComponent } from './affiche-correction-quizz.component';

describe('AfficheCorrectionQuizzComponent', () => {
  let component: AfficheCorrectionQuizzComponent;
  let fixture: ComponentFixture<AfficheCorrectionQuizzComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AfficheCorrectionQuizzComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AfficheCorrectionQuizzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
