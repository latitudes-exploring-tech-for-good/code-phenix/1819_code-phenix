import { Component, OnInit, Input } from '@angular/core';
import {Quizz} from '../quizz';

@Component({
  selector: 'app-affiche-correction-quizz',
  templateUrl: './affiche-correction-quizz.component.html',
  styleUrls: ['./affiche-correction-quizz.component.css']
})
export class AfficheCorrectionQuizzComponent implements OnInit {

  @Input() quizzCorrige : Quizz;
  
  constructor() { }

  ngOnInit() {
  }

  getClass(vraie : Boolean){
    if(vraie){
      return "border border-success rounded"
    }
    else{
      return "border border-danger rounded"
    }
  }
}
