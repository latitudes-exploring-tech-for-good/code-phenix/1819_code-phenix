import { Component, OnInit, Input } from '@angular/core';
import { QuizzService } from '../quizz.service';
import { Quizz } from '../quizz';

@Component({
  selector: 'app-affiche-quizz',
  templateUrl: './affiche-quizz.component.html',
  styleUrls: ['./affiche-quizz.component.css']
})
export class AfficheQuizzComponent implements OnInit {

  @Input() quizzId : String;  // l'id du quizz concerné est renseigné par input dans liste-quizz

  quizz : Quizz;  // le texte du quizz concerné
  quizzCorrige : Quizz;
  correction = false;

  constructor( private quizzService : QuizzService) { }

  ngOnInit() {
    this.getQuizz(this.quizzId); // récupère le quizz concerné au lancement du component (quand on ouvre la page)
  }

  getQuizz(id : String): void {  // pour récupérer la valeur du quizz dont l'id est id.
    this.quizzService.getQuizz(id)
    .subscribe(quizz => 
      {this.quizz = quizz});
  }

  submit() {
    console.log(this.quizz);
    this.corrigeQuizz(this.quizz)
  }

  corrigeQuizz(quizz : Quizz){
    this.correction = true;
    this.quizz = null;
    this.quizzService.corrigeQuizz(quizz)
    .subscribe(quizz => {this.quizzCorrige = quizz})

  }

  refaire(){
    this.correction = false;
    this.getQuizz(this.quizzId);
    this.quizzCorrige = null;
  }
}
