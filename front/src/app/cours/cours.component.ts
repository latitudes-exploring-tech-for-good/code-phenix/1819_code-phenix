import { Component, OnInit } from '@angular/core';
import {CoursService} from '../cours.service';
import {Cours} from '../cours';
import { HttpClient } from "@angular/common/http";
import { AuthenticationService}  from '../authentication.service';

@Component({
  selector: 'app-cours',
  templateUrl: './cours.component.html',
  styleUrls: ['./cours.component.css']
})
export class CoursComponent implements OnInit {
  
   // on initialise la variable cours qui est ce qui va être affiché par le composant
  listeCours : Cours[];

  display : String;

  coursSelect : Cours;

  isAdmin : Boolean = this.authService.isAdmin();
    
  onSelect(cours : Cours): void{
        this.coursSelect = cours;
        this.display = 'detail';
    }



  constructor(private coursService: CoursService, private http: HttpClient, 
    private authService : AuthenticationService) {
   
   }

   getCours(): void {
    console.log('on appelle la fontion getCours');
    this.coursService.getCours().subscribe(data => {
      console.log(data);
    this.listeCours = data;}
       );
    console.log("la  liste des cours obtenus est " + this.listeCours);
  }

  displayCreationCours(){
    this.display = 'creation';

  }

  displayCoursList(){
    this.getCours();
    this.display = 'cours';
    
  }

  ngOnInit() {
    this.getCours();
    this.display = 'cours';
  }

}
