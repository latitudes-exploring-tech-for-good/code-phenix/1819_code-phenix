import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './/app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { CoursComponent } from './cours/cours.component';
import {CoursService} from './cours.service';
import {AuthInterceptorService} from './auth-interceptor.service';
import {  HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthenticationService} from './authentication.service';
import {AuthGuard} from './authguard.service';
import { HomeAdminComponent } from './home-admin/home-admin.component';
import {AdminGuardService} from './admin-guard.service';
import { ProfilComponent } from './profil/profil.component';
import { CreationCoursComponent } from './creation-cours/creation-cours.component';
import { AccueilUserComponent } from './accueil-user/accueil-user.component';
import { AfficheQuizzComponent } from './affiche-quizz/affiche-quizz.component';
import { CreationQuizzComponent } from './creation-quizz/creation-quizz.component';
import { ListeQuizzComponent } from './liste-quizz/liste-quizz.component';
import { CoursDetailComponent } from './cours-detail/cours-detail.component';
import { AfficheCorrectionQuizzComponent } from './affiche-correction-quizz/affiche-correction-quizz.component';
import { ChapitreComponent } from './chapitre/chapitre.component';
import { CreationComponent } from './creation/creation.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    CoursComponent,
    HomeAdminComponent,
    ProfilComponent,
    CreationCoursComponent,
    AccueilUserComponent,
    AfficheQuizzComponent,
    CreationQuizzComponent,
    ListeQuizzComponent,
    AfficheCorrectionQuizzComponent,
    CreationComponent,
    CoursDetailComponent,
    AfficheCorrectionQuizzComponent,
    ChapitreComponent
  ],
  imports: [
    BrowserModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [CoursService,
    AuthGuard,
    AdminGuardService,
    AuthenticationService,
    ListeQuizzComponent,
    {
    provide:HTTP_INTERCEPTORS,
    useClass: AuthInterceptorService,
    multi : true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }