import { Injectable } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';
import { HttpRequest } from '@angular/common/http';
import { HttpHandler } from '@angular/common/http';
import { HttpEvent } from '@angular/common/http';
import {Observable} from 'rxjs'; 


// ce service permet, pour chaque requête du front, d'ajouter, si l'utilisateur est connecté, le token à la requête

@Injectable({
  providedIn: 'root'
})

export class AuthInterceptorService implements HttpInterceptor {

  intercept(req: HttpRequest<any>,
            next: HttpHandler): Observable<HttpEvent<any>> {

     console.log("requête interceptée");

      const idToken = localStorage.getItem("id_token");

      if (idToken) {
          const cloned = req.clone({
              headers: req.headers.set("Authorization",
                  "Bearer " + idToken)
          });

          return next.handle(cloned);
      }
      else {
          return next.handle(req);
      }
  }
}