import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { User } from "./user";
import * as jwt_decode from "jwt-decode";
import * as moment from 'moment';
import { ReturnStatement } from "@angular/compiler";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  loginUrl = 'http://localhost:3000/api/users/login'
  incorrect = false;
     
    constructor(private http: HttpClient) {
    }
      
    login(username:string, password:string ) {
        try{ 
          return this.http.post<User>(this.loginUrl, {username, password}) }
        catch(e){
        }
        // dans le tuto l'appel à setSession se faisait ici mais cela ne fonctionne pas --> on l'appelle dans login.component.ts    
    }

    readToken(authResult){
      // cette fonction décode un token et retourne les infos qu'il contient
      const jwt = authResult.token
      const tokenInfo = jwt_decode(jwt)
      console.log(tokenInfo);
      return tokenInfo; 
    }

    public readInfo(){
      const tokenInfo = jwt_decode(localStorage.getItem("id_token"));
      return tokenInfo; 
    }

    setSession(authResult){
      // cette fonction crée une session à un utilisateur en stockant son token d'accès et sa date d'expiration
      try{
        const expiresAt = moment().add(this.readToken(authResult).exp, 'second');
        const jwt = authResult.token
        localStorage.setItem('id_token', jwt); 
        localStorage.setItem('expiresAt', JSON.stringify(expiresAt.valueOf()));
        localStorage.setItem('is_admin',this.readToken(authResult).admin);
        console.log("La session a demarrée et expire dans " + localStorage.getItem('expiresAt') + " seconds");
        console.log("le token est " + localStorage.getItem('id_token') );
        console.log('admin :',localStorage.getItem('is_admin'));
        this.incorrect = false;
      }
      catch(e){    // gérer le cas ou les identifiants sont faux (le BACK renvoit alors un token non valable ce qui
                   // provoque une erreur)
        this.incorrect = true;
      }

    }

    logout(){
      //permet de se déconnecter en supprimant les infos de la session
      localStorage.removeItem("id_token");
      localStorage.removeItem("expiresAt");
      localStorage.removeItem('is_admin');
    }

    public isLoggedIn(){
      // booléen indiquant si l'utilisateur est tjrs connecté
      //return moment().isBefore(this.getExpiration())
      return !this.isTokenExpired(localStorage.getItem("id_token"));

    }

    public isLoggedOut(){
      return !this.isLoggedIn();
    }

    public isAdmin(): Boolean {
      return ( localStorage.getItem('is_admin') === 'true'); 
    }

   /*  getExpiration(){
      const expiration = localStorage.getItem('expires_at')
      const expiresAt = JSON.parse(expiration)
      return moment(expiresAt);
    } */

    getToken(): string {
      return localStorage.getItem("id_token");
    }

    getExpiration(token: string): Date {
      const decoded = jwt_decode(token);
  
      if (decoded.exp === undefined) return null;
  
      const date = new Date(0); 
      date.setUTCSeconds(decoded.exp);
      return date;
    }
  
    isTokenExpired(token?: string): boolean {
      if(!token) token = this.getToken();
      if(!token) return true;
  
      const date = this.getExpiration(token);
      if(date === undefined) return false;
      return !(date.valueOf() > new Date().valueOf());
    }


}