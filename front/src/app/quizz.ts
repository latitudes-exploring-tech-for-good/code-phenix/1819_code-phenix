export class Quizz {
    _id : string;
    nom : string;
    score : Number;
    questions : [{
        _id: string;
        question : string;
        correction : Boolean;
        multipleChoice : Boolean;
        reponses : [{ enonce : string;
                     vraie : Boolean;
                    reponse : Boolean}]
    }]
}