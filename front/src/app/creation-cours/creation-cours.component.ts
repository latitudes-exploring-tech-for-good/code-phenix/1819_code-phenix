import { Component, OnInit } from '@angular/core';
import {CoursService} from '../cours.service';
import {Cours} from '../cours';

@Component({
  selector: 'app-creation-cours',
  templateUrl: './creation-cours.component.html',
  styleUrls: ['./creation-cours.component.css']
})
export class CreationCoursComponent implements OnInit {

  constructor(private coursService : CoursService ) { }

  public newCours: Cours = new Cours()


    create() {
      this.coursService.createCours(this.newCours)
        .subscribe((res) => {
          this.newCours = new Cours()
        })
    }
  

  ngOnInit() {
  }

}
