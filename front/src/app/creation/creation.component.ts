import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-creation',
  templateUrl: './creation.component.html',
  styleUrls: ['./creation.component.css']
})
export class CreationComponent implements OnInit {

  display = 'boutons';

  constructor() { }

  ngOnInit() {
  }

  onSelect(dsp : string) {
    this.display = dsp;
  }

}
