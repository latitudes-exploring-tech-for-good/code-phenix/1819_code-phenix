# 1819_code-phenix

https://wiki.latitudes.cc/Code_Phenix_(2018-2019)

Etapes pour utiliser le programme : 

- copier le lien http du projet (quand on clique sur "cloner" dans gitlab)

- cloner le projet locallement grâce à la commande git clone adresseurl (attention cela crée un nouveau dossier qui contient le projet)

- installer les modules avec npm install dependencies

// pour lancer le front : 

- intaller angular dans le sous dossier front avec la commande ng install -g angular/cli

- Toujours dans le front installer le moduler suivant : npm install --save-dev @angular-devkit/build-angular

- lancer le programme avec npm start

Normalement, la console indique qu'il faut se connecter sur l'adresse localhost:4200 : c'est que le projet fonctionne. 

// pour démarrer le back : 

- lancer la commande npm start (dans une ligne de commande séparée de celle du front)